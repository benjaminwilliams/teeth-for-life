# Teeth for life - Static page


A code test from Gee Multimedia for Ben Williams.

Everything needed to deploy the site is in the /site/ directory.


## Dependencies & Usage

The site is totally static and can be run without a web server.

However, to build the Sass files you will need [node and npm](http://nodejs.org/)

Browse to the root folder in terminal then run:

        npm install

This will in install all the plugins needed to run gulp. Then run:

        gulp

To run all the gulp tasks found in /gulpfile.js
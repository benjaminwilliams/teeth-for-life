var gulp = require('gulp');
var sass = require('gulp-sass');
var prefix  = require('gulp-autoprefixer');
var concat = require('gulp-concat');
var sequence= require('run-sequence');
var rimraf = require('rimraf');
var minify = require('gulp-minify-css');
var rename = require('gulp-rename');

/**
 * Tasks
 */
gulp.task('clean', function(cb) {
    rimraf('./site/css', cb);
});

gulp.task('minify-css', function() {
    gulp.src('./site/css/styles.css')
        .pipe(minify())
        .pipe(rename('styles.min.css'))
        .pipe(gulp.dest('./site/css'))
});

gulp.task('sass', function(){
    gulp.src([
        'src/styles/reset.scss',    // Get the rest first
        'src/styles/**/*.scss'         // Then get everything else
        ])
        .pipe(sass())               // Convert from Sass to CSS
        .pipe(concat('styles.css')) // Concatenate into a single file
        .pipe(prefix(['last 2 versions', 'ie 8', 'ie 9']))  // prefix for up to date vendor prefixes
        .pipe(gulp.dest('site/css'));    // and place in the /css folder
});


/**
 * Default task
 */
gulp.task('default', function() {
    sequence(       // Sequence ensures files complete in order and don't fall over each other
        'clean',
        'sass',
        'minify-css'
    );
});


/**
 * Dev Task that doesn't minify styles for debugging
 */
gulp.task('dev', function() {
    sequence(
        ['clean'],
        ['sass']
    );
});


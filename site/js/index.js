"use strict";

/**
 * Only run the carousel plugin when needed.
 * Checks to see if the carousel is currently visible
 */
var enableCarousel = function(){
    if($(".show-carousel").css("display") == "block" ){
        $(".js-toggle-carousel").addClass("owl-carousel");
        $('.owl-carousel').owlCarousel({
            nav:true,
            items: 1
        });
    }
    else {
        $(".js-toggle-carousel").removeClass("owl-carousel");
    }
};

/**
 *  Toggle mobile menu
 */
$(".js-menu-toggle").click(function(){
    if($(".js-nav-menu").hasClass("is-open")){
        $(".js-nav-menu").removeClass("is-open");
    }
    else {
        $(".js-nav-menu").addClass("is-open");
    }
});
$(".js-nav-overlay").click(function(){ // A crude overlay that allows us to have somewhere to click to close the menu
    $(".js-nav-menu").removeClass("is-open");
});

$(document).ready(function(){
    enableCarousel();
});

$(window).resize(function(){
    // We might need to run the plugin on a window resize.
    enableCarousel();
});